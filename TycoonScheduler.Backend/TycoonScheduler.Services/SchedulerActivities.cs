﻿using Microsoft.EntityFrameworkCore;
using TycoonScheduler.Model;
using TycoonScheduler.Model.Exceptions;
using TycoonScheduler.Persistence;

namespace TycoonScheduler.Services
{
    // </inheritdoc>
    public class SchedulerActivities : ISchedulerActivities
    {
        private readonly ProductContext _productContext;

        public SchedulerActivities(ProductContext productContext) { 
            _productContext = productContext;
        }

        public async Task<IEnumerable<Product>> GetProducts(DateTime start, DateTime end)
        {
            return await _productContext.Products
                    .Include(o => o.Workers)
                    .Where( prod => start <= prod.StartTime 
                            && end >= prod.StartTime)
                    .ToListAsync();
        }

        public async Task<IEnumerable<Product>> GetAllProducts()
        {
            return await _productContext.Products
                            .Include(o => o.Workers)
                            .ToListAsync();
        }

        public async Task<Product> GetProduct(int productId)
        {
            return await _productContext.Products
                        .Include(o => o.Workers)
                        .FirstOrDefaultAsync(prod => prod.Id == productId) 
                            ?? throw new ItemNotFoundException($"Unable to find product with Id: {productId}");
        }

        public async Task<IEnumerable<Worker>> GetAllWorkers()
        {
            return await _productContext.Workers
                            .Include( o => o.AssociatedProducts)
                            .ToListAsync();
        }

        public async Task<Worker> GetWorkerById(char Id)
        {
            return await _productContext.Workers
                    .FirstOrDefaultAsync(worker => worker.Id == Id)
                        ?? throw new ItemNotFoundException($"Unable to find worker with Id: {Id}");
        }

        public async Task<IEnumerable<Product>> BuildActivity(ProductType productType, DateTime start, DateTime end)
        {

            if(start > end)
            {
                throw new InvalidOperation("Start date cannot be after end date");
            }

            var product = new Product()
            {
                ProductType = productType,
                StartTime = start,
                EndTime = end,
            };

            await _productContext.Products.AddAsync(product);
            await _productContext.SaveChangesAsync();
            return await _productContext.Products.ToListAsync();
        }
        

        public async Task<IEnumerable<Worker>> CreateWorker(char Id)
        {
            await _productContext.Workers.AddAsync(new Worker() { Id = Id});
            await _productContext.SaveChangesAsync();
            return await _productContext.Workers.ToListAsync();
        }

        public async Task<Product> AddWorkersToProduct(int productId, List<char> workerId)
        {
            var product = await GetProduct(productId);

            if(product.ProductType == ProductType.Component
                && (workerId.Count > 1 || product.Workers.Count > 0))
            {
                throw new InvalidOperation("More than one worker not allowed in building compoent.");
            }

            var workers = await ToWorkerList(workerId);

            if (workers.TrueForAll(worker => ValidateWorkerAssignment(product, worker)))
            {
                product.Workers = workers;
            }

            _productContext.SaveChanges();

            return await GetProduct(productId);
        }

        public async Task<Product> UpdateSchedule(int productId, DateTime? start, DateTime? end)
        {
            var product = await GetProduct(productId);
            product.StartTime = start ?? product.StartTime;
            product.EndTime = start ?? product.EndTime;

            _productContext.SaveChanges();

            return await GetProduct(productId);
        }

        public async Task<IEnumerable<Product>> DeleteActivity(int productId)
        {
            var product = await GetProduct(productId);

            _productContext.Products.Remove(product);
            await _productContext.SaveChangesAsync();

            return await GetAllProducts();
        }

        public async Task<IEnumerable<Worker>> GetTopBusyAndroid(int top = 10)
        {
            var workers = await GetAllWorkers();
            var busyWorkers = workers.OrderByDescending(worker => worker.WeekHour).Take(top);
            return await Task.FromResult(busyWorkers);

        }

        private async Task<List<Worker>> ToWorkerList(List<char> workersId)
        {
            var workerList = new List<Worker>();

            foreach (var workerId in workersId)
            {
                workerList.Add(await GetWorkerById(workerId));

            }

            return await Task.FromResult(workerList);
        }

        private bool ValidateWorkerAssignment(Product product, Worker worker)
        {

            foreach (var associatedProduct in worker.AssociatedProducts)
            {
                if(product.StartTime >= associatedProduct.StartTime 
                    && product.StartTime <= associatedProduct.EndTimeIncludingRest)
                {
                    throw new InvalidOperation
                        ($"Worker {worker.Id} cannot be assigned to " +
                        $"activity {product.Id} as the worker is blocked or resting.");
                }
            }

            return true;
        }


    }
}
