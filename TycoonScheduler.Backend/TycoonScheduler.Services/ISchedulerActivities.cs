﻿using TycoonScheduler.Model;

namespace TycoonScheduler.Services
{
    /// <summary>
    /// The methods required for scheduling.
    /// </summary>
    public interface ISchedulerActivities
    {
        Task<IEnumerable<Product>> BuildActivity(ProductType productType, DateTime start, DateTime end);
        Task<IEnumerable<Product>> GetProducts(DateTime start, DateTime end);
        Task<IEnumerable<Product>> GetAllProducts();
        Task<Product> GetProduct(int productId);
        Task<IEnumerable<Worker>> GetAllWorkers();
        Task<Worker> GetWorkerById(char Id);
        Task<Product> AddWorkersToProduct(int productId, List<char> workerId);
        Task<IEnumerable<Worker>> CreateWorker(char Id);

        Task<Product> UpdateSchedule(int productId, DateTime? start, DateTime? end);
        Task<IEnumerable<Product>> DeleteActivity(int productId);

        Task<IEnumerable<Worker>> GetTopBusyAndroid(int top = 10);
    }
}