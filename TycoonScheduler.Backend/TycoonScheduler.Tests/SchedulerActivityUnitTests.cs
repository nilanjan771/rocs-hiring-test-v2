using TycoonScheduler.Model;
using TycoonScheduler.Model.Exceptions;
using TycoonScheduler.Services;
using Xunit;

namespace TycoonScheduler.Tests
{
    public class SchedulerActivityUnitTests : IClassFixture<TestDatabaseFixture>
    {
        public SchedulerActivityUnitTests(TestDatabaseFixture fixture)
                => Fixture = fixture;

        public TestDatabaseFixture Fixture { get; }

        [Fact]
        public async Task Get_Activites_GivenDate()
        {
            using var context = Fixture.CreateContext();
            var schedulerActivities = new SchedulerActivities(context);

            var products = await schedulerActivities
                .GetProducts(Convert.ToDateTime("03-11-2023 00:00:00"), 
                                Convert.ToDateTime("07-11-2023 00:00:00"));

            Assert.True(products.Count() == 6);
        }

        [Fact]
        public async Task Get_NoActivites_GivenDate()
        {
            using var context = Fixture.CreateContext();
            var schedulerActivities = new SchedulerActivities(context);

            var products = await schedulerActivities
                .GetProducts(Convert.ToDateTime("04-11-2024 00:00:00"),
                                Convert.ToDateTime("05-11-2024 00:00:00"));

            Assert.Empty(products);
        }

        [Fact]
        public async Task Assign_ExistingWorkersToComponent_Succesfully()
        {
            using var context = Fixture.CreateContext();
            var schedulerActivities = new SchedulerActivities(context);

            var product =  await schedulerActivities
                                   .AddWorkersToProduct(2, new List<char> { 'D' });

            Assert.NotNull(product);
            Assert.Equal('D', product.Workers[0].Id);

        }

        [Fact]
        public async Task Assign_NonExistingWorkersToComponent_ItemNotFoundException()
        {
            using var context = Fixture.CreateContext();
            var schedulerActivities = new SchedulerActivities(context);

            var ex = await Record.ExceptionAsync(
                           async () => await schedulerActivities
                                   .AddWorkersToProduct(2, new List<char> { 'x' }));

            Assert.NotNull(ex);
            Assert.IsType<ItemNotFoundException>(ex);
            Assert.Contains("Unable to find worker", ex.Message);

        }

        [Fact]
        public async Task Assign_TwoWorkersToComponent_ThrowsWorkerLimitException()
        {
            using var context = Fixture.CreateContext();
            var schedulerActivities = new SchedulerActivities(context);

            var ex = await Record.ExceptionAsync(
                async () => await schedulerActivities
                        .AddWorkersToProduct(1, new List<char> { 'A', 'B' }));

            Assert.NotNull(ex);
            Assert.IsType<InvalidOperation>(ex);
            Assert.Contains("More than one worker not allowed in building compoent", ex.Message);
        }

        [Fact]
        public async Task Assign_BlockedWorkersToComponent_ThrowsWorkerBlockedException()
        {
            using var context = Fixture.CreateContext();
            var schedulerActivities = new SchedulerActivities(context);

            var products = await schedulerActivities.AddWorkersToProduct(1, new List<char> { 'A' });

            Assert.IsType<Product>(products);
            Assert.True(products.Workers[0].Id == 'A');

            var ex = await Record.ExceptionAsync(
                async () => await schedulerActivities
                        .AddWorkersToProduct(2, new List<char> { 'A' }));

            Assert.NotNull(ex);
            Assert.IsType<InvalidOperation>(ex);
            Assert.Contains("worker is blocked or resting", ex.Message);
        }

        [Fact]
        public async Task Get_Top_Workers()
        {
            using var context = Fixture.CreateContext();
            var schedulerActivities = new SchedulerActivities(context);
            await schedulerActivities
                                  .AddWorkersToProduct(2, new List<char> { 'D' });
            var products = await schedulerActivities.GetTopBusyAndroid();

            Assert.NotEmpty(products);
        }
    }
}