﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata;
using System.Text;
using System.Threading.Tasks;
using TycoonScheduler.Model;
using TycoonScheduler.Persistence;

namespace TycoonScheduler.Tests
{
    public class TestDatabaseFixture
    {
        private const string ConnectionString = @"Server=(localdb)\\MSSQLLocalDB;Database=ProductContextDb;Trusted_Connection=True;";

        private static readonly object _lock = new();
        private static bool _databaseInitialized;

        public TestDatabaseFixture()
        {
            lock (_lock)
            {
                if (!_databaseInitialized)
                {
                    using (var context = CreateContext())
                    {
                        context.Database.EnsureDeleted();
                        context.Database.EnsureCreated();
                        context.Workers.AddRange(FakeWorkers());
                        context.Products.AddRange(FakeProducts());
                        context.SaveChanges();
                    }

                    _databaseInitialized = true;
                }
            }
        }

        public ProductContext CreateContext()
            => new ProductContext(
                new DbContextOptionsBuilder<ProductContext>()
                    .UseSqlServer(ConnectionString)
                    .Options);

        private List<Worker> FakeWorkers()
        {
            return new List<Worker>()
            {
                new Worker()
                {
                    Id = 'A'
                },
                new Worker()
                {
                    Id = 'B'
                },
                new Worker()
                {
                    Id = 'C'
                },
                new Worker()
                {
                    Id = 'D'
                },
                new Worker()
                {
                    Id = 'Y'
                },
                new Worker()
                {
                    Id = 'Z'
                }
            };
        }

        private List<Product> FakeProducts()
        {
            return new List<Product>()
            {
                new Product()
                {
                    ProductType = ProductType.Component,
                    StartTime = Convert.ToDateTime("04-11-2023 00:00:00"),
                    EndTime = Convert.ToDateTime("04-11-2023 02:00:00")
                },
                new Product()
                {
                    ProductType = ProductType.Component,
                    StartTime = Convert.ToDateTime("04-11-2023 02:00:00"),
                    EndTime = Convert.ToDateTime("04-11-2023 03:00:00")
                },
                new Product()
                {
                    ProductType = ProductType.Component,
                    StartTime = Convert.ToDateTime("05-11-2023 00:00:00"),
                    EndTime = Convert.ToDateTime("05-11-2023 02:00:00")
                },
                new Product()
                {
                    ProductType = ProductType.Component,
                    StartTime = Convert.ToDateTime("06-11-2023 00:00:00"),
                    EndTime = Convert.ToDateTime("06-11-2023 02:00:00")
                },
                new Product()
                {
                    ProductType = ProductType.Machine,
                    StartTime = Convert.ToDateTime("05-11-2023 00:00:00"),
                    EndTime = Convert.ToDateTime("05-11-2023 02:00:00")
                },
                new Product()
                {
                    ProductType = ProductType.Machine,
                    StartTime = Convert.ToDateTime("06-11-2023 00:00:00"),
                    EndTime = Convert.ToDateTime("06-11-2023 02:00:00")
                }
            };
        }
    }
}
