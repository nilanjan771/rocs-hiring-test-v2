﻿using Microsoft.EntityFrameworkCore;
using System.Reflection.Metadata;
using TycoonScheduler.Model;

namespace TycoonScheduler.Persistence
{
    public class ProductContext : DbContext
    {
        public DbSet<Worker> Workers { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<ProductWorker> ProductWorker { get; set; }

        public string DbPath { get; }

        public ProductContext(DbContextOptions<ProductContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Worker>()
            .HasMany(e => e.AssociatedProducts)
            .WithMany(e => e.Workers)
            .UsingEntity<ProductWorker>();

        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer("Server=(localdb)\\MSSQLLocalDB;Database=ProductContextDb;Trusted_Connection=True;");
        }
    }
}