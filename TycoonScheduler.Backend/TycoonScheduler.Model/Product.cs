﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TycoonScheduler.Model
{
    /// <summary>
    /// Schedules a Product(Machine or Component) for building.
    /// </summary>
    public class Product
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public ProductType ProductType { get; set; }
        public List<Worker> Workers { get; set; } = new();

        [NotMapped]
        public BuildStatus BuildStatus
        { 
            get {
                if(DateTime.Now < StartTime)
                {
                    return BuildStatus.Scheduled;
                }
                else if (DateTime.Now < EndTime)
                {
                    return BuildStatus.InProgress;
                }
                return BuildStatus.Completed;
            } 
        }

        [NotMapped]
        public DateTime EndTimeIncludingRest
        {
            get
            {
                return this.ProductType == ProductType.Machine ? this.EndTime.AddHours(4) : this.EndTime.AddHours(2);
            }
        }

        [NotMapped]
        public int HourSpan {
            get { 
                return (this.EndTime - this.StartTime).Hours;
            }
        }
    }

    public enum ProductType
    {
        Component,
        Machine
    }

    public enum BuildStatus
    {
        Scheduled,
        InProgress,
        Completed
    }
}
