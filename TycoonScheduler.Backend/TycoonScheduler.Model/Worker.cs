﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics.CodeAnalysis;

namespace TycoonScheduler.Model
{
    /// <summary>
    /// Class/Table holding Worker information.
    /// </summary>
    public class Worker
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int AutoId { get; set; }

        [Required]
        [NotNull]
        public char Id { get; set; }

        // Product currently the worker is working on
        // null suggest that no task is assigned to the worker.
        public List<Product> AssociatedProducts { get; set; } = new();

        [NotMapped]
        public int WeekHour { 
            get
            {
                var x = AssociatedProducts.Where(
                    p => DateTime.Now < p.StartTime && p.StartTime < DateTime.Now.AddDays(7));

                return x.Sum(o => o.HourSpan);
            }
        }

    }
}
