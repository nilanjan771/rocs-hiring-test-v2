﻿/// Domain exception models.
/// TODO: Add exception handling middleware 
/// to change them into appropriate status code.
namespace TycoonScheduler.Model.Exceptions
{
    public class ItemNotFoundException : Exception
    {
        public ItemNotFoundException(string message) : base(message)
        {
                
        }
    }

    public class DuplicateItem : Exception
    {
        public DuplicateItem(string message) : base(message)
        {

        }
    }

    public class InvalidOperation : Exception
    {
        public InvalidOperation(string message) : base(message)
        {

        }
    }
}
