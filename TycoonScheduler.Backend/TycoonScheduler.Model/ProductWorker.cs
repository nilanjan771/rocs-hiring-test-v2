﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TycoonScheduler.Model
{
    public class ProductWorker
    {
        [Key]
        public int Id { get; set; }

        [ForeignKey("Product.Id")]
        public int ProductId { get; set; }
        [ForeignKey("Worker.AutoId")]
        public int WorkerId { get; set; }
    }
}
