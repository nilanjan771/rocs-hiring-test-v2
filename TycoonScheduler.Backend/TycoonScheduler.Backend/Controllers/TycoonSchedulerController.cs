using Microsoft.AspNetCore.Mvc;
using TycoonScheduler.Model;
using TycoonScheduler.Services;

namespace TycoonScheduler.Backend.Controllers
{
    /// <summary>
    /// End points to schedule Activities for Androids.
    /// </summary>
    [ApiController]
    [Route("[controller]")]
    public class TycoonSchedulerController : ControllerBase
    {

        private readonly ILogger<TycoonSchedulerController> _logger;
        private readonly ISchedulerActivities _schedulerActivities;

        public TycoonSchedulerController(
            ISchedulerActivities schedulerActivities,
            ILogger<TycoonSchedulerController> logger)
        {
            _schedulerActivities = schedulerActivities;
            _logger = logger;
        }

        /// <summary>
        /// returns List of all activities</returns>
        /// </summary>

        [HttpGet("all-activites")]
        public async Task<IActionResult> GetActivites()
        {
            return Ok(await _schedulerActivities.GetAllProducts());
        }

        /// <summary>
        /// returns List of all workers</returns>
        /// </summary>

        [HttpGet("all-workers")]
        public async Task<IActionResult> GetWorkers()
        {
            return Ok(await _schedulerActivities.GetAllWorkers());
        }

        /// <summary>
        /// Queries activities between the given start and end date time.
        /// </summary>
        /// <returns>List of activities</returns>
        [HttpGet("activites/between")]
        public async Task<IActionResult> GetActivites(DateTime start, DateTime end)
        {
            return Ok(await _schedulerActivities.GetProducts(start, end));
        }

        /// <summary>
        /// Schedule activity to build component.
        /// </summary>
        /// <param name="productType">Schedule a product (Component, Machine)</param>
        /// <param name="start">start time.</param>
        /// <param name="end">end time.</param>
        /// <returns>Details of created component.</returns>
        [HttpPost("schedule/build/{productType}")]
        public async Task<IActionResult> ScheduleBuildProduct(
            ProductType productType, DateTime start, DateTime end)
        {
            return Ok(await _schedulerActivities.BuildActivity(productType, start, end));
        }

        /// <summary>
        /// Creates a unique worker,
        /// that can be associated with a product (Component, Machine).
        /// </summary>
        /// <param name="workerId"></param>
        /// <returns>Worker details</returns>
        [HttpPost("initialize/android/{workerId}")]
        public async Task<IActionResult> CreateWorker(char workerId)
        {
            return Ok(await _schedulerActivities.CreateWorker(workerId));
        }

        /// <summary>
        /// Assign android to product
        /// </summary>
        /// <param name="productId"></param>
        /// <param name="workerId"></param>
        /// <returns></returns>
        [HttpPatch("assign/android-to/{productId}")]
        public async Task<IActionResult> CreateWorker(int productId, List<char> workerId)
        {
            return Ok(await _schedulerActivities.AddWorkersToProduct(productId, workerId));
        }

        /// <summary>
        /// Updates the start or end time for given product.
        /// </summary>
        /// <returns>List of activities</returns>
        [HttpPatch("update-activity/{productId}")]
        public async Task<IActionResult> UpdateActivitySchedule(int productId, DateTime start, DateTime end)
        {
            return Ok(await _schedulerActivities.UpdateSchedule(productId, start, end));
        }

        /// <summary>
        /// Delete given product.
        /// </summary>
        /// <returns>List of activities</returns>
        [HttpDelete("delete-activity/{productId}")]
        public async Task<IActionResult> DeleteActivity(int productId)
        {
            return Ok(await _schedulerActivities.DeleteActivity(productId));
        }

        /// <summary>
        /// returns a list of the top 10 androids that are
        /// working more time in the next 7 days</returns>
        /// </summary>

        [HttpGet("top-workers")]
        public async Task<IActionResult> GetTopBusyAndroid()
        {
            return Ok(await _schedulerActivities.GetTopBusyAndroid());
        }
    }
}