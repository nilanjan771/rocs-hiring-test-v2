# Ryanair TravelLabs - ROCS Hiring Test - Tycoon Factory

## Solution Approach
- The primary approach behind the solution was API first.
- Most of the features that are expected have an API created which was tested using postman.
- The solution Is a basic implementation of clean architecture
- -The code base is seggreated into multiple projects to apply seperation of concern
- - Unit test is added and the ActivityService is Unit Tested as that contained the meat of the implementation.
	- Domain exception classes have been introduced but
	- implementation of middle ware to convert exceptions to appropriate Rest Status codes is left
- EF Core is used for persistence.

- There has been significant evolution in the DB design 
- - initial approach I took was to normalize the tables into types of Product
	- Later changed it into single table and distinguish by a flag.
- Directly used the DbContext into service layer, instead of creating a repository for simplicity.


